#include "VirtualMachine.h"
#include "Machine.h"
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <algorithm>
#include <cstring>
#include <cstdint>
#include <fcntl.h>


using namespace std;

extern "C"{
	//Functions used from utils
	void VMStringCopyN(...);
	void VMStringCopy(...);
	void VMStringConcatenate(...);



	int ticksPerMS;
	volatile TVMTick tickCount = 0;
	const TVMMemoryPoolID VM_MEMORY_POOL_ID_SYSTEM = 0;
	const TVMMemoryPoolID VM_MEMORY_POOL_ID_SHARED = 1;

	struct Mutex;

	typedef struct TCB{
		TVMMemorySize memsize;
		TVMThreadPriority prio;
		TVMThreadEntry entry;
		volatile TVMThreadState state;
		TVMThreadID tid;
		void *param;
		void* stack;
		SMachineContextRef context;

		int flag;

		bool qUp = 0;

		bool waitingForIO;
		int IOResult = -1;

		volatile int mutexWait;

		Mutex* waitingOn;

		volatile int q;

		volatile unsigned int sleep;

		bool operator<(const TCB& rhs) const{
			if(flag == 1)
				return true;
			if(prio == rhs.prio)
				return q > rhs.q;
			return prio < rhs.prio;
		}
	} TCB;

	typedef struct Mutex{
		bool locked;
		TVMThreadIDRef owner;
		TVMMutexID mid;
		vector<TCB*> highQueue;
		vector<TCB*> midQueue;
		vector<TCB*> lowQueue;
	} Mutex;

	typedef struct MemoryBlock{
		uint8_t* start;
		uint8_t* end;
		unsigned int size;
	} MemoryBlock;

	typedef struct MemoryPool{
		unsigned int size;
		uint8_t* base;
		TVMMemoryPoolID mpid;

		vector<MemoryBlock> memoryBlocks;
	} MemoryPool;

	TVMMainEntry VMLoadModule(const char *module);
	TVMMainEntry VMUnloadModule();
	void machineFileWriteCallback(void* calledParam, int result);
	void machineAlarmCallback(void* callData);
	TCB* getThreadByID(TVMThreadID);
	void threadSkeleton(void* t);
	void scheduler();
	void idleEntry(void*);

	struct MyComparator {
  		bool operator() (TCB* arg1, TCB* arg2) {
    		return *arg1 < *arg2; //calls your operator
  		}
	};

	//Threading Stuff 
	Mutex* getMutexByID(TVMMutexID);
	void removeFromMutex(TCB*);
	vector<TCB*> threads;	
	priority_queue<TCB*, vector<TCB*>, MyComparator> readyQueue;
	TCB* current;

	volatile int q = 0;

	TVMThreadID nextTID = 1;

	//Mutex Stuff
	vector<Mutex*> muts;
	TVMMutexID nextMID = 1;
	void* sharedBase;

	//Memory Pool Stuff
	vector<MemoryPool*> memoryPools;
	TVMMemoryPoolID nextMPID = 1;

	//FAT stuff
	typedef struct BPB{
		uint8_t BS_jmpBoot[4];
		char BS_OEM_Name[9];	
		uint16_t BPB_BytsPerSec;
		uint8_t BPB_SecPerClus;
		uint16_t BPB_RsvdSecCnt;
		uint8_t BPB_NumFATs;
		uint16_t BPB_RootEntCnt;
		uint16_t BPB_TotSec16;
		uint8_t BPB_Media;
		uint16_t BPB_FATSz16;
		uint16_t BPB_SecPerTrk;
		uint16_t BPB_NumHeads;
		uint32_t BPB_HiddSec;
		uint32_t BPB_TotSec32;
		uint8_t BS_DrvNum;
		uint8_t BS_Reserved1;
		uint8_t BS_BootSig;
		uint32_t BS_VolID;
		char BS_VolLab[12];
		uint8_t BS_FilSysType[9];
	} BPB;

	typedef struct FileEntry{
		char DIR_Name[13];
		char DIR_Ext[4];
		uint8_t DIR_Attr;
		uint8_t DIR_NTRes;
		uint8_t DIR_CrtTimeTenth;
		uint16_t DIR_CrtTime;
		uint16_t DIR_CrtDate;
		uint16_t DIR_LstAccDate;
		uint16_t DIR_FstClusHI;
		uint16_t DIR_WrtTime;
		uint16_t DIR_WrtDate;
		uint16_t DIR_FstClusLO;
		uint16_t DIR_FileSize;
	} FileEntry;

	typedef struct DirEnt{
		unsigned int ID;
		unsigned int loc;
	} DirEnt;

	typedef struct FileEnt{
		unsigned int ID;
		unsigned int loc;
		unsigned int inLoc;
	} FileEnt;

	const char* mntloc;
	int fat;
	BPB bpb;
	uint16_t* FAT;
	//first data sector= data start (address)
	//read a cluster, sections per clusrter
	//given memory two seeks and two reads

	vector<FileEntry> root;	//files and directories in root

	vector<DirEnt> openDirs;
	vector<FileEnt> openFiles;
	int FirstDataSector;
	int ClusterCount;

	unsigned int currentFD = 3;

	void readBPB(void* mem){
		memcpy(bpb.BS_jmpBoot, (uint8_t*)mem + 0, 3);
		bpb.BS_jmpBoot[3] = '\0';
		memcpy(bpb.BS_OEM_Name, (uint8_t*)mem + 3, 8);	
		bpb.BS_OEM_Name[8] = '\0';
		memcpy(&bpb.BPB_BytsPerSec, (uint8_t*)mem + 11, 2);
		memcpy(&bpb.BPB_SecPerClus, (uint8_t*)mem + 13, 1);
		memcpy(&bpb.BPB_RsvdSecCnt, (uint8_t*)mem + 14, 2);
		memcpy(&bpb.BPB_NumFATs, (uint8_t*)mem + 16, 1);
		memcpy(&bpb.BPB_RootEntCnt, (uint8_t*)mem + 17, 2);
		memcpy(&bpb.BPB_TotSec16, (uint8_t*)mem + 19, 2);
		memcpy(&bpb.BPB_Media, (uint8_t*)mem + 21, 1);
		memcpy(&bpb.BPB_FATSz16, (uint8_t*)mem + 22, 2);
		memcpy(&bpb.BPB_SecPerTrk, (uint8_t*)mem + 24, 2);
		memcpy(&bpb.BPB_NumHeads, (uint8_t*)mem + 26, 2);
		memcpy(&bpb.BPB_HiddSec, (uint8_t*)mem + 28, 4);
		memcpy(&bpb.BPB_TotSec32, (uint8_t*)mem + 32, 4);
		memcpy(&bpb.BS_DrvNum, (uint8_t*)mem + 36, 1);
		memcpy(&bpb.BS_Reserved1, (uint8_t*)mem + 37, 1);
		memcpy(&bpb.BS_BootSig, (uint8_t*)mem + 38, 1);
		memcpy(&bpb.BS_VolID, (uint8_t*)mem + 39, 4);
		memcpy(bpb.BS_VolLab, (uint8_t*)mem + 43, 11);
		bpb.BS_VolLab[11] = '\0';
		memcpy(bpb.BS_FilSysType, (uint8_t*)mem + 54, 8);
		bpb.BS_FilSysType[8] = '\0';

		//Verification
		////////////printf("OEM Name: %s\n", bpb.BS_OEM_Name);
		////////////printf("Bytes per sector: %hu\n", bpb.BPB_BytsPerSec);
		////////////printf("Sectors per cluster: %hu\n", bpb.BPB_SecPerClus);
		////////////printf("Reserved Sectors: %hu\n", bpb.BPB_RsvdSecCnt);
		////////////printf("Number of FATs: %hu\n", bpb.BPB_NumFATs);
		////////////printf("Directory Entry Count: %hu\n", bpb.BPB_RootEntCnt);
		////////////printf("Total Sectors(16): %hu\n", bpb.BPB_TotSec16);
		////////////printf("Media: %hu\n", bpb.BPB_Media);
		////////////printf("Fat Size 16: %hu\n", bpb.BPB_FATSz16);
		////////////printf("Sectors per track: %hu\n", bpb.BPB_SecPerTrk);
		////////////printf("Number Heads: %hu\n", bpb.BPB_NumHeads);
		////////////printf("Number hidden sectors: %hu\n", bpb.BPB_HiddSec);
		////////////printf("Number Sectors(32): %u\n", bpb.BPB_TotSec32);
		////////////printf("Drive Num: %hu\n", bpb.BS_DrvNum);
		////////////printf("BootSig: %hu\n", bpb.BS_BootSig);
		////////////printf("Volume ID: %hu\n", bpb.BS_VolID);
		////////////printf("Volume Label  %s\n", bpb.BS_VolLab);
		////////////printf("File Sys: %s\n", bpb.BS_FilSysType);
	}

	TVMStatus VMStart(int tickms, TVMMemorySize heapSize, TVMMemorySize sharedSize, const char* mount, int argc, char *argv[]){
		sharedBase = MachineInitialize(sharedSize);

		ticksPerMS = tickms;
		MachineRequestAlarm(1000*tickms, &machineAlarmCallback, NULL);

		TVMMainEntry module = VMLoadModule(argv[0]);

		//mntloc
		mntloc = mount;

		//Create main thread
		TCB* main = new TCB;
		main->state = VM_THREAD_STATE_RUNNING;
		main->prio = VM_THREAD_PRIORITY_NORMAL;
		main->sleep = 0;
		main->context = new SMachineContext;
		main->waitingForIO = 0;
		main->flag = 7;

		current = main;

		threads.push_back(main);

		MemoryPool* mainMemoryPool = new MemoryPool; 
		mainMemoryPool->mpid = VM_MEMORY_POOL_ID_SYSTEM;
		mainMemoryPool->size = heapSize;
		mainMemoryPool->base = new uint8_t[heapSize];
		memoryPools.push_back(mainMemoryPool);

		MemoryBlock start;
		start.end = mainMemoryPool->base;
		start.start = mainMemoryPool->base;
		start.size = 0;
		mainMemoryPool->memoryBlocks.push_back(start);
		MemoryBlock end;
		end.start = mainMemoryPool->base + mainMemoryPool->size;
		end.end = mainMemoryPool->base + mainMemoryPool->size;
		end.size = 0;
		mainMemoryPool->memoryBlocks.push_back(end);

		MemoryPool* sharedMem = new MemoryPool;
		sharedMem->mpid = nextMPID++;
		sharedMem->size = sharedSize;
		sharedMem->base = (uint8_t*)sharedBase;
		start.end = sharedMem->base;
		start.start = sharedMem->base;
		start.size = 0;
		sharedMem->memoryBlocks.push_back(start);
		end.start = sharedMem->base + sharedMem->size;
		end.end = sharedMem->base + sharedMem->size;
		end.size = 0;
		sharedMem->memoryBlocks.push_back(end);
		memoryPools.push_back(sharedMem);

		//Create idle thread
		TVMThreadID idleID;
		VMThreadCreate(idleEntry, NULL, 0x100000, VM_THREAD_PRIORITY_LOW, &idleID);
		VMThreadActivate(idleID);
		getThreadByID(idleID)->flag = 1;


		//Mount the image
		void *mem;
		VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem); //Allocate memory for the shared read

		current->state = VM_THREAD_STATE_WAITING;
		MachineFileOpen(mntloc, O_RDWR, 0600, &machineFileWriteCallback, (void*)current);
		scheduler();

		fat = current->IOResult;

		////////////printf("Got FD for FAT\n");

		current->state = VM_THREAD_STATE_WAITING;
		MachineFileRead(fat, mem, 512, &machineFileWriteCallback, (void*)current);
		scheduler();

		////////////printf("Read %d bytes of data from FAT\n", current->IOResult);
		////////////printf("BPB Info\n");
		readBPB(mem);

		int FirstRootSector = bpb.BPB_RsvdSecCnt + bpb.BPB_NumFATs * bpb.BPB_FATSz16;
		unsigned int RootDirectorySectors = (bpb.BPB_RootEntCnt * 32) / 512;
		FirstDataSector = FirstRootSector + RootDirectorySectors;
		ClusterCount = (bpb.BPB_TotSec32 - FirstDataSector) / bpb.BPB_SecPerClus;

		////////////printf("First Root Sector: %d\n", FirstRootSector);
		////////////printf("Root Dir Sectors: %d\n", RootDirectorySectors);
		////////////printf("First Data Sector: %d\n", FirstDataSector);
		////////////printf("Cluster Count: %d\n", ClusterCount);

		FAT = new uint16_t[ClusterCount+2];

		int j = ClusterCount+2;

		for(unsigned int sec = 0; sec < bpb.BPB_FATSz16; sec++){
			current->state = VM_THREAD_STATE_WAITING;
			MachineFileSeek(fat, sec*512+512, SEEK_SET, &machineFileWriteCallback, (void*)current);
			scheduler();

			current->state = VM_THREAD_STATE_WAITING;
			MachineFileRead(fat, mem, 512, &machineFileWriteCallback, (void*)current);
			scheduler();

			for(int r = 0; r < 256; r++){
				memcpy(&FAT[r+sec*256], (uint8_t*)mem + (r*2), 2);
				j--;
				if(j == 0){
					////////////printf("Stopping on %d\n", r+sec*256);
					break;
				}
			}
		}

		////////////printf("Finished reading FAT\n");

		////////////printf("Reading Root Entries\n");

		for(unsigned int i = 0; i < RootDirectorySectors; i++){
			bool stop = false;
			current->state = VM_THREAD_STATE_WAITING;
			MachineFileSeek(fat, (i*512)+(512*FirstRootSector), SEEK_SET, &machineFileWriteCallback, (void*)current);
			scheduler();

			current->state = VM_THREAD_STATE_WAITING;
			MachineFileRead(fat, mem, 512, &machineFileWriteCallback, (void*)current);
			scheduler();

			////////printf("Reading sector %d\n", i);

			for(j = 0; j < 16; j++){
				FileEntry e;
				VMStringCopyN(e.DIR_Name, (uint8_t*) mem + 32*j, 8);
				if(e.DIR_Name[0] == '\0'){
					stop = true;
					break;
				}
				////////printf("Cont\n");
				int r = 0;
				while(e.DIR_Name[r] != '\0' && e.DIR_Name[r] != 0x20){
					r++;
				}
				e.DIR_Name[r] = '\0';
				VMStringCopyN(e.DIR_Ext, (uint8_t*) mem + 32*j + 8, 3);
				memcpy(&e.DIR_Attr, (uint8_t*) mem + 32*j + 11, 1); 
				memcpy(&e.DIR_NTRes, (uint8_t*) mem + 32*j + 12, 1); 
				memcpy(&e.DIR_CrtTimeTenth, (uint8_t*) mem + 32*j + 13, 1); 
				memcpy(&e.DIR_CrtTime, (uint8_t*) mem + 32*j + 14, 2); 
				memcpy(&e.DIR_CrtDate, (uint8_t*) mem + 32*j + 16, 2); 
				memcpy(&e.DIR_LstAccDate, (uint8_t*) mem + 32*j + 18, 2); 
				memcpy(&e.DIR_FstClusHI, (uint8_t*) mem + 32*j + 20, 2); 
				memcpy(&e.DIR_WrtTime, (uint8_t*) mem + 32*j + 22, 2); 
				memcpy(&e.DIR_WrtDate, (uint8_t*) mem + 32*j + 24, 2); 
				memcpy(&e.DIR_FstClusLO, (uint8_t*) mem + 32*j + 26, 2); 
				memcpy(&e.DIR_FileSize, (uint8_t*) mem + 32*j + 28, 4); 
				if((e.DIR_Attr & 0x10) == 0 && e.DIR_Ext[0] != ' '){
					VMStringConcatenate(e.DIR_Name, ".");
					VMStringConcatenate(e.DIR_Name, e.DIR_Ext);
					r = 0;
					while(e.DIR_Name[r] != '\0' && e.DIR_Name[r] != 0x20){
						r++;
					}
					e.DIR_Name[r] = '\0';
				}
				root.push_back(e);
			}
			if(stop)
				break;
		}

		for(unsigned int i = 0; i < root.size(); i++){
			if(root[i].DIR_Attr != (0x1 | 0x2 | 0x4 | 0x8)){
				////////////printf("%s",root[i].DIR_Name);
				if((root[i].DIR_Attr & 0x10) > 0){
					////////////printf("\t<DIR>");
				}
				else{
					////////////printf(".%s\t<FILE>", root[i].DIR_Ext);
				}
				////////////printf("\t%d\n", root[i].DIR_FileSize);
			}
		}

		VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, &mem); //Deallocate 

		if(module){
			module(argc, argv);
			VMUnloadModule();
			int j = ClusterCount+2;

			//Save FAT updates
			for(unsigned int sec = 0; sec < bpb.BPB_FATSz16; sec++){
				current->state = VM_THREAD_STATE_WAITING;
				MachineFileSeek(fat, sec*512+512, SEEK_SET, &machineFileWriteCallback, (void*)current);
				scheduler();

				for(int r = 0; r < 256; r++){
					memcpy((uint8_t*)mem + (r*2), &FAT[r+sec*256], 2);
					j--;
					if(j == 0){
						break;
					}
				}

				current->state = VM_THREAD_STATE_WAITING;
				MachineFileWrite(fat, mem, 512, &machineFileWriteCallback, (void*)current);
				scheduler();
			}

			//Save root files
			for(unsigned int i = 0; i < root.size(); i++){
				FileEntry fd = root[i];
				if(fd.DIR_Attr != (0x1 | 0x2 | 0x4 | 0x8)){
					int rootSec = i / 16;
					////////printf("%d\n", rootSec);
					//The 0th thru 15th files will be in first (32*16 = 512)
					int offset = i % 16;
					////////printf("%d\n", offset);

					current->state = VM_THREAD_STATE_WAITING;
					MachineFileSeek(fat, (rootSec*512)+(512*FirstRootSector) + offset * 32, SEEK_SET, &machineFileWriteCallback, (void*)current);
					scheduler();

					uint8_t buffer[32];
					memset(buffer, 0, 32);

					char* temp = strdup(fd.DIR_Name);
					char* name = strtok(temp, ".");
					VMStringCopyN(fd.DIR_Name, name, 8);
					for(int j = 0; j < 13; j++){
						if(fd.DIR_Name[j] == '\0'){
							memset(&fd.DIR_Name[j], ' ', 13-j);
							break;
						}
					}
					fd.DIR_Name[11] = '\0';

					////////printf("Writing file: %s\n", fd.DIR_Name);


					char* ext = strtok(NULL, ".");
					if(ext != NULL){
						VMStringCopyN(&fd.DIR_Name[8], ext, 3);
					}
					free(temp);

					memcpy(buffer, fd.DIR_Name, 11);
					memcpy((uint8_t*) buffer+ 11, &fd.DIR_Attr, 1); 
					memcpy((uint8_t*) buffer+ 14, &fd.DIR_CrtTime, 2); 
					memcpy((uint8_t*) buffer+ 16, &fd.DIR_CrtDate, 2); 
					memcpy((uint8_t*) buffer+ 18, &fd.DIR_LstAccDate, 2); 
					memcpy((uint8_t*) buffer+ 20, &fd.DIR_FstClusHI, 2); 
					memcpy((uint8_t*) buffer+ 22, &fd.DIR_WrtTime, 2); 
					memcpy((uint8_t*) buffer+ 24, &fd.DIR_WrtDate, 2); 
					memcpy((uint8_t*) buffer+ 26, &fd.DIR_FstClusLO, 2); 
					memcpy((uint8_t*) buffer+ 28, &fd.DIR_FileSize, 4);
					memcpy(mem, buffer, 32);

					////////printf("%c\n", buffer[0]);

					current->state = VM_THREAD_STATE_WAITING;
					MachineFileWrite(fat, mem, 32, &machineFileWriteCallback, (void*)current);
					scheduler();
				}
			}

			MachineTerminate();
			delete[] FAT;
			return VM_STATUS_SUCCESS;
		}
		else
			return VM_STATUS_FAILURE;
	}

	TVMStatus VMThreadSleep(TVMTick tick){
		if(VM_TIMEOUT_INFINITE == tick)
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;
		current->sleep = tick;

		if(VM_TIMEOUT_IMMEDIATE == tick){
			//////////////////printf("sleep set to 0\n");
			current->sleep = 0; 
			current->state = VM_THREAD_STATE_READY;
			current->q = q++;
			readyQueue.push(current);	
		}
		
		scheduler();

		MachineResumeSignals(&state);

		return VM_STATUS_SUCCESS;
	}

	//Thread Commands
	TVMStatus VMThreadCreate(TVMThreadEntry entry, void *param, TVMMemorySize memsize, TVMThreadPriority prio, TVMThreadIDRef tid){
		TMachineSignalState state;
		MachineSuspendSignals(&state);		
		if(NULL == entry || NULL == tid){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}
		
		TCB *thread = new TCB;
		thread->prio = prio;
		thread->memsize = memsize;
		thread->entry = entry;
		thread->param = param;
		thread->state = VM_THREAD_STATE_DEAD;
		thread->sleep = 0;
		(*tid) = thread->tid = nextTID++;
		thread->flag = nextTID+100;
		thread->waitingForIO = 0;

		VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SYSTEM, thread->memsize, &thread->stack);

		threads.push_back(thread);	

		MachineResumeSignals(&state);
	
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMThreadDelete(TVMThreadID thread){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		TCB* t = getThreadByID(thread);
		if(NULL == t){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_ID;
		}
		if(VM_THREAD_STATE_DEAD != t->state){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}
		VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SYSTEM, t->stack);
		threads.erase(std::remove(threads.begin(), threads.end(), t), threads.end());
		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMThreadActivate(TVMThreadID thread){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		TCB* t = getThreadByID(thread);
		
		if(NULL == t){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_ID;
		}
		
		if(VM_THREAD_STATE_DEAD != t->state){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}

		t->state = VM_THREAD_STATE_READY;


		t->context = new SMachineContext;

		MachineContextCreate(t->context, threadSkeleton, t, t->stack, t->memsize);

		t->q = q++;

		readyQueue.push(t);

		scheduler();

		MachineResumeSignals(&state);

		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMThreadTerminate(TVMThreadID thread){
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		TCB* t = getThreadByID(thread);


		if(t == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_ID;
		}

		if(t->state == VM_THREAD_STATE_DEAD){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}

		t->state = VM_THREAD_STATE_DEAD;
		removeFromMutex(t);

		scheduler();

		MachineResumeSignals(&state);

		return VM_STATUS_SUCCESS;

	}

	TVMStatus VMThreadID(TVMThreadIDRef threadref){
		if(threadref != NULL){
			(*threadref) = current->tid;
			return VM_STATUS_SUCCESS;
		}
		else
			return VM_STATUS_ERROR_INVALID_PARAMETER;
	}

	TVMStatus VMThreadState(TVMThreadID thread, TVMThreadStateRef stateref){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		TCB* t = getThreadByID(thread);	

		if(NULL == stateref){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		if(t != NULL){
			(*stateref) = t->state;
			MachineResumeSignals(&state);
			return VM_STATUS_SUCCESS;
		}

		MachineResumeSignals(&state);

		return VM_STATUS_ERROR_INVALID_ID;
	}

	TCB* getThreadByID(TVMThreadID tid){
		for(auto it = threads.begin(); it != threads.end(); ++it){
			if(tid == (*it)->tid){
				return *it;
			}
		}
		return NULL;
	}

	TVMStatus VMTickMS(int *tickmsref){
		if(tickmsref != NULL){
			*tickmsref = ticksPerMS;
			return VM_STATUS_SUCCESS;
		}
		else
			return VM_STATUS_ERROR_INVALID_PARAMETER ;
	}

	TVMStatus VMTickCount(TVMTickRef tickref){
		if(tickref != NULL){
			(*tickref) = tickCount;
			return VM_STATUS_SUCCESS;
		}
		else
			return VM_STATUS_ERROR_INVALID_PARAMETER ;
	}

	void threadSkeleton(void* t){
		MachineEnableSignals();
		TCB* tr = (TCB*)t;
		tr->entry(tr->param);
		VMThreadTerminate(tr->tid);
	}

	void idleEntry(void * t){
		MachineEnableSignals();
		while(true){
		}
	}

	TVMStatus VMFileOpen(const char *filename, int flags, int mode, int *filedescriptor){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;

		if(filename == NULL || filedescriptor == NULL){
			MachineResumeSignals (&state);
  			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		//MachineFileOpen(filename, flags, mode, &machineFileWriteCallback, (void*)t);

		////printf("%s name\n", filename);

		bool found = false;
		unsigned int i = 0;
		for(;i < root.size(); i++){
			////printf("%s name\n", root[i].DIR_Name);
			if(strcmp(filename, root[i].DIR_Name) == 0 && root[i].DIR_Attr != 0x10 && root[i].DIR_Attr != (0x1 | 0x2 | 0x4 | 0x8)){
				found = true;
				break;
			}
		}

		*filedescriptor = -1;
		if(found){
			FileEnt f;
			f.ID = ++currentFD;
			f.loc = i;
			f.inLoc = 0;
			openFiles.push_back(f);
			*filedescriptor = f.ID;
		}
		else{
			if((flags & O_CREAT) == 0){
				MachineResumeSignals (&state);
				return VM_STATUS_FAILURE;
			}
			//////////printf("Creating %s in root\n", filename);
			FileEntry fe;
			SVMDateTime dt;
			VMDateTime(&dt);

			//////////printf("xxxxx\n");

			char* temp = strdup(filename);
			char* name = strtok(temp, ".");
			VMStringCopyN(fe.DIR_Name, name, 8);

			//////////printf("%s\n", fe.DIR_Name);

			char* ext = strtok(NULL, ".");
			if(ext != NULL){
				VMStringCopyN(fe.DIR_Ext, ext, 3);
				//////////printf("%s\n", fe.DIR_Ext);
			}
			free(temp);
			
			VMStringCopy(fe.DIR_Name, filename);

			//Find the first free block
			int j;
			for(j = 0; j < ClusterCount; j++){
				if(FAT[j] == 0){
					break;
				}
			}

			fe.DIR_FstClusHI = 0;
			fe.DIR_FstClusLO = j;
			fe.DIR_FileSize = 0;
			fe.DIR_Attr = 0;
			
			FAT[j] = 0xFFFF;


			FileEnt f;
			f.ID = ++currentFD;
			f.loc = i;
			f.inLoc = 0;
			openFiles.push_back(f);
			root.push_back(fe);
			*filedescriptor = f.ID;		
		}
		//scheduler();

		//*filedescriptor = t->IOResult;
		MachineResumeSignals(&state);
		if(*filedescriptor < 0){
			MachineResumeSignals(&state);
			return VM_STATUS_FAILURE;
		}


		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMFileRead(int filedescriptor, void *data, int *length){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		TCB *t = current;

		if (data == NULL || length == NULL){
			MachineResumeSignals (&state);
  			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		void *mem;

		int written = 0;

		int i = 0;
		int j = *length;

		if(filedescriptor <= 3){
			current->state = VM_THREAD_STATE_WAITING;
			while(j > 512){
				while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
					current->state = VM_THREAD_STATE_WAITING;
					scheduler();
				}
				MachineFileRead(filedescriptor, (uint8_t*)mem, 512, &machineFileWriteCallback, (void*)t);
				current->state = VM_THREAD_STATE_WAITING;
				scheduler();
				VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);
				j -= 512;
				i += 512;
				written += t->IOResult;
				memcpy((uint8_t*)data + i, mem, 512);
			}

			while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, j, &mem)){
				current->state = VM_THREAD_STATE_WAITING;
				scheduler();
			}
			MachineFileRead(filedescriptor, (uint8_t*)mem, j, &machineFileWriteCallback, (void*)t);
			current->state = VM_THREAD_STATE_WAITING;
			scheduler();
			VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);

			*length = t->IOResult + written;

			memcpy((uint8_t*)data + i, mem, j);
		}
		else{
			bool found = false;
			for(unsigned int i = 0; i < openFiles.size(); i++){
				if(filedescriptor == (int)openFiles[i].ID){
					found = true;
					break;
				}
			}

			if(!found){
				MachineResumeSignals (&state);
				return VM_STATUS_ERROR_INVALID_PARAMETER;
			}

			if(openFiles[i].inLoc >= root[openFiles[i].loc].DIR_FileSize){
				MachineResumeSignals (&state);
				return VM_STATUS_FAILURE;
			}

			int fileSize = root[openFiles[i].loc].DIR_FileSize;

			int start = root[openFiles[i].loc].DIR_FstClusLO;

			int x = openFiles[i].inLoc;

			int open = i;


			while(x >= 1024){
				x -= 1024;
				start = FAT[start];
			}


			j = *length;


			int readCount = 0; //When this is 2 we get the next cluster
			//First, if we are not on a cluster boundary we must do a special thing
			if(x != 0){
				while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
					current->state = VM_THREAD_STATE_WAITING;
					scheduler();
				}

				current->state = VM_THREAD_STATE_WAITING;
				MachineFileSeek(fat, FirstDataSector*512 + (start-2)*1024 + x, SEEK_SET, &machineFileWriteCallback, (void*)current);
				scheduler();

				current->state = VM_THREAD_STATE_WAITING;
				//Read part of the remaining sector
				unsigned int readAmount;
				if(x >= 512){
					readAmount = 1024 - x;
					start = FAT[start];
				}
				else{
					readAmount = 512 - x;
					readCount = 1; //We are on the second of the cluster for the next read
				}

				if((int)readAmount > *length)
					readAmount = *length;
				if(readAmount > fileSize - openFiles[i].inLoc)
					readAmount = fileSize - openFiles[i].inLoc;

				MachineFileRead(fat, mem, readAmount, &machineFileWriteCallback, (void*)current);
		
				i += readAmount;
				j -= readAmount;

				scheduler();

				written += t->IOResult;

				memcpy((uint8_t*)data, mem, i);

				VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);


				current->state = VM_THREAD_STATE_WAITING;
			}


			//While we have a full buffer to read
			while(j >= 512){
				//If we are trying to read 512, but we dont have that much left!
				unsigned int readAmount = 512;
				if(readAmount > fileSize - openFiles[open].inLoc - i){
					break;
				}

				while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
					current->state = VM_THREAD_STATE_WAITING;
					scheduler();
				}

				current->state = VM_THREAD_STATE_WAITING;
				MachineFileSeek(fat, FirstDataSector*512 + (start-2)*1024 + readCount * 512, SEEK_SET, &machineFileWriteCallback, (void*)current);
				scheduler();


				current->state = VM_THREAD_STATE_WAITING;
				MachineFileRead(fat, mem, 512, &machineFileWriteCallback, (void*)current);
				scheduler();

				memcpy((uint8_t*)data + i, mem, 512);

				j -= 512;
				i += 512;
				VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);

				written += t->IOResult;

				readCount++;
				if(readCount == 2){
					readCount = 0;
					start = FAT[start];
				}
			}


			while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
				current->state = VM_THREAD_STATE_WAITING;
				scheduler();
			}

			current->state = VM_THREAD_STATE_WAITING;
			MachineFileSeek(fat, FirstDataSector*512 + (start-2)*1024 + readCount * 512, SEEK_SET, &machineFileWriteCallback, (void*)current);
			scheduler();

			unsigned int readAmount = j;
			if(readAmount > fileSize - openFiles[open].inLoc - i)
				readAmount = fileSize - openFiles[open].inLoc - i;

			current->state = VM_THREAD_STATE_WAITING;
			MachineFileRead(fat, mem, readAmount, &machineFileWriteCallback, (void*)current);
			scheduler();

			memcpy((uint8_t*)data + i, mem, readAmount);

			i += readAmount;
			j -= readAmount;

			VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);


			written += t->IOResult;

			openFiles[open].inLoc += i;
			*length = written;

		}
		

		MachineResumeSignals(&state);
		if(*length < 0){
			MachineResumeSignals (&state);
			return VM_STATUS_FAILURE;
		}


		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMFileWrite(int filedescriptor, void *data, int *length){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		current->state = VM_THREAD_STATE_WAITING;
		TCB *t = current;

		if (data == NULL || length == NULL){
			MachineResumeSignals (&state);
  			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		void *mem;

		int written = 0;

		int i = 0;
		int j = *length;

		if(filedescriptor <= 3){
			while(j > 512){
				while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
					current->state = VM_THREAD_STATE_WAITING;
					scheduler();
				}
				memcpy(mem, (uint8_t*)data + i, 512);
				MachineFileWrite(filedescriptor, (uint8_t*)mem, 512, &machineFileWriteCallback, (void*)t);
				current->state = VM_THREAD_STATE_WAITING;
				scheduler();
				VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);
				j -= 512;
				i += 512;
				written += t->IOResult;
			}

			while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, j, &mem)){
				current->state = VM_THREAD_STATE_WAITING;
				scheduler();
			}
			memcpy(mem, (uint8_t*)data + i, j);
			MachineFileWrite(filedescriptor, (uint8_t*)mem, j, &machineFileWriteCallback, (void*)t);
			current->state = VM_THREAD_STATE_WAITING;
			scheduler();
			VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);

			*length = t->IOResult + written;
		}
		else{
			bool found = false;
			for(unsigned int i = 0; i < openFiles.size(); i++){
				if(filedescriptor == (int)openFiles[i].ID){
					found = true;
					break;
				}
			}

			if(!found){
				MachineResumeSignals (&state);
				return VM_STATUS_ERROR_INVALID_PARAMETER;
			}

			if(openFiles[i].inLoc > root[openFiles[i].loc].DIR_FileSize){
				MachineResumeSignals (&state);
				return VM_STATUS_FAILURE;
			}

			int start = root[openFiles[i].loc].DIR_FstClusLO;
	

			int x = openFiles[i].inLoc;
			
			int open = i;

			while(x >= 1024){
				x -= 1024;
				start = FAT[start];
			}


			j = *length;

			int readCount = 0; //When this is 2 we get the next cluster
			//First, if we are not on a cluster boundary we must do a special thing
			if(x != 0){
				while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
					current->state = VM_THREAD_STATE_WAITING;
					scheduler();
				}

				current->state = VM_THREAD_STATE_WAITING;
				MachineFileSeek(fat, FirstDataSector*512 + (start-2)*1024 + x, SEEK_SET, &machineFileWriteCallback, (void*)current);
				scheduler();

				current->state = VM_THREAD_STATE_WAITING;
				//Read part of the remaining sector
				memcpy(mem,(uint8_t*)data, 512);
				if(x >= 512){
					//Find the first free block
					int blk;
					for(blk = 0; j < ClusterCount; j++){
						if(FAT[blk] == 0){
							break;
						}
					}

					if(FAT[start] >= 0xFFF8){
						FAT[start] = blk;
						FAT[blk] = 0xFFFF;
						start = blk;
					}
					else{
						start = FAT[start];
					}
					i += 1024-x;
					j -= 1024-x;
					MachineFileWrite(fat, mem, 1024-x, &machineFileWriteCallback, (void*)current);
				}
				else{
					readCount = 1; //We are on the second of the cluster for the next read
					i += 512 - x;
					j -= 512 - x;
					MachineFileWrite(fat, mem, 512-x, &machineFileWriteCallback, (void*)current);
				}

				scheduler();
				written += t->IOResult;
				VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);
				current->state = VM_THREAD_STATE_WAITING;
			}


			//While we have a full buffer to read
			while(j >= 512){
				while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
					current->state = VM_THREAD_STATE_WAITING;
					scheduler();
				}

				current->state = VM_THREAD_STATE_WAITING;
				MachineFileSeek(fat, FirstDataSector*512 + (start-2)*1024 + readCount * 512, SEEK_SET, &machineFileWriteCallback, (void*)current);
				scheduler();

				current->state = VM_THREAD_STATE_WAITING;
				memcpy(mem,(uint8_t*)data + i, 512);
				MachineFileWrite(fat, mem, 512, &machineFileWriteCallback, (void*)current);
				scheduler();			
				
				j -= 512;
				i += 512;
				VMMemoryPoolDeallocate(VM_MEMORY_POOL_ID_SHARED, mem);

				written += t->IOResult;

				readCount++;
				if(readCount == 2){
					readCount = 0;
					if(j > 0 && FAT[start] >= 0xFFF8){
						//Find the first free block
						int blk;
						for(blk = 0; j < ClusterCount; j++){
							if(FAT[blk] == 0){
								break;
							}
						}

						FAT[start] = blk;
						FAT[blk] = 0xFFFF;
						start = blk;
					}
					else if(j > 0 && FAT[start] < 0xFFF8){
						start = FAT[start];
					}
				}
			}

			while(VM_STATUS_ERROR_INSUFFICIENT_RESOURCES == VMMemoryPoolAllocate(VM_MEMORY_POOL_ID_SHARED, 512, &mem)){
				current->state = VM_THREAD_STATE_WAITING;
				scheduler();
			}


			current->state = VM_THREAD_STATE_WAITING;
			MachineFileSeek(fat, FirstDataSector*512 + (start-2)*1024 + readCount * 512, SEEK_SET, &machineFileWriteCallback, (void*)current);
			scheduler();

			current->state = VM_THREAD_STATE_WAITING;
			memcpy(mem, (uint8_t*) data + i, j);
			MachineFileWrite(fat, mem, j, &machineFileWriteCallback, (void*)current);
			scheduler();			

			i += j;
			j -= j;

			written += t->IOResult;

			openFiles[open].inLoc += i;
			*length = written;

			if(root[openFiles[open].loc].DIR_FileSize < openFiles[open].inLoc){
				////////printf("Updating file size\n");
				root[openFiles[open].loc].DIR_FileSize = openFiles[open].inLoc;
			}
		}
		MachineResumeSignals(&state);
		if(*length < 0)
			return VM_STATUS_FAILURE;


		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMFileSeek(int filedescriptor, int offset, int whence, int *newoffset){
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		for(unsigned int i = 0; i < openFiles.size(); i++){
			if(filedescriptor == (int)openFiles[i].ID){
				openFiles[i].inLoc = offset;
				*newoffset = offset;
				MachineResumeSignals(&state);
				return VM_STATUS_SUCCESS;
			}
		}

		MachineResumeSignals(&state);
		return VM_STATUS_FAILURE;
	}

	TVMStatus VMFileClose(int filedescriptor){
		////////////printf("Closing file\n");
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		for(unsigned int i = 0; i < openFiles.size(); i++){
			if(filedescriptor == (int)openFiles[i].ID){
				openFiles.erase(openFiles.begin()+i);
				MachineResumeSignals(&state);
				return VM_STATUS_SUCCESS;
			}
		}

		MachineResumeSignals (&state);
		return VM_STATUS_FAILURE;
	}

	void scheduler(){
		if(readyQueue.empty())
			return;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		TCB* top = readyQueue.top();

		while(VM_THREAD_STATE_DEAD == top->state){
			readyQueue.pop();
			if(readyQueue.empty()){
				MachineResumeSignals (&state);
				return;
			}
			else
				top = readyQueue.top();
		}

		if(top->prio > current->prio || (top->prio == current->prio && current->qUp) || current->state != VM_THREAD_STATE_RUNNING){
			TCB* old = current;
			readyQueue.pop();

			//printf("Switch from %d", current->flag);
			current->qUp = 0;
			current = top;

			if(old->state == VM_THREAD_STATE_RUNNING && current != old){
				old->state = VM_THREAD_STATE_READY;
				old->q = q++;
				readyQueue.push(old);
			}
			//printf(" to %d\n", current->flag);


			current->state = VM_THREAD_STATE_RUNNING;

			MachineContextSwitch(old->context, current->context);
		}
		MachineResumeSignals (&state);
	}

	//Mutex commands

	TVMStatus VMMutexCreate(TVMMutexIDRef mutexref){
		if(mutexref == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		
		Mutex* m = new Mutex;
		m->locked = false;
		m->owner = NULL;
		(*mutexref) = m->mid = nextMID++;
		muts.push_back(m);
		
		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;	
	}

	TVMStatus VMMutexDelete(TVMMutexID mutex){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		Mutex* m = getMutexByID(mutex);
		if(NULL == m){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_ID;
		}
		if(m->locked){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}
		muts.erase(std::remove(muts.begin(), muts.end(), m), muts.end());
		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMutexQuery(TVMMutexID mutex, TVMThreadIDRef ownerref){
		Mutex* m = getMutexByID(mutex);
		
		if(m == NULL)
			return VM_STATUS_ERROR_INVALID_ID;
		
		if(ownerref == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		if(m->owner == NULL){
			(*ownerref) = VM_THREAD_ID_INVALID; 
		}
		else{
			(*ownerref) = (*m->owner);
		}

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;	
	}

	TVMStatus VMMutexAcquire(TVMMutexID mutex, TVMTick timeout){
		Mutex* m = getMutexByID(mutex);
		if(NULL == m)
			return VM_STATUS_ERROR_INVALID_ID;

		TMachineSignalState state;
		MachineSuspendSignals(&state);


		if(m->locked == false){
			m->locked = true;
			m->owner = &current->tid;
			current->waitingOn = m;
			MachineResumeSignals(&state);
			return VM_STATUS_SUCCESS;
		}
		else if(VM_TIMEOUT_IMMEDIATE == timeout){
			MachineResumeSignals(&state);
			return VM_STATUS_FAILURE;
		}
		
		int wait = timeout;

		if(VM_TIMEOUT_INFINITE == timeout){
			wait = -1;
		}
		
		current->mutexWait = wait;
		switch(current->prio){
			case VM_THREAD_PRIORITY_LOW:
				m->lowQueue.push_back(current);
				break;
			case VM_THREAD_PRIORITY_NORMAL:
				m->midQueue.push_back(current);
				break;
			case VM_THREAD_PRIORITY_HIGH:
				m->highQueue.push_back(current);
				break;
		}
		
		current->state = VM_THREAD_STATE_WAITING;
		current->mutexWait = wait;
		current->waitingOn = m;

		scheduler();

		if((*m->owner) == current->tid){
			MachineResumeSignals(&state);
			return VM_STATUS_SUCCESS;	
		}
		else{
			MachineResumeSignals(&state);
			return VM_STATUS_FAILURE;	
		}
	}

	TVMStatus VMMutexRelease(TVMMutexID mutex){
		Mutex* m = getMutexByID(mutex);
		if(m == NULL)
			return VM_STATUS_ERROR_INVALID_ID;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		current->waitingOn = NULL;
		////////////////printf("res\n");

		//////printf("%d is releasing\n", current->flag);
		
		if((*m->owner) == current->tid){
			m->owner = NULL;
			m->locked = false;
			if(!m->highQueue.empty()){
				TCB* owner = m->highQueue[0];	
				m->owner = &owner->tid;
				m->highQueue.erase(m->highQueue.begin());
				owner->mutexWait = 0;
				owner->state = VM_THREAD_STATE_READY;
				owner->q = q++;
				readyQueue.push(owner);
			}
			else if(!m->midQueue.empty()){
				TCB* owner = m->midQueue[0];	
				m->owner = &owner->tid;
				m->midQueue.erase(m->midQueue.begin());
				owner->mutexWait = 0;
				owner->state = VM_THREAD_STATE_READY;
				owner->q = q++;
				readyQueue.push(owner);
			}
			else if(!m->lowQueue.empty()){
				TCB* owner = m->lowQueue[0];	
				m->owner = &owner->tid;
				m->lowQueue.erase(m->lowQueue.begin());
				owner->mutexWait = 0;
				owner->state = VM_THREAD_STATE_READY;
				owner->q = q++;
				readyQueue.push(owner);
			}
			else{
				//////printf("Handing to low proc\n");
				m->owner = NULL;
				m->locked = false;
			}

			scheduler();
		}
		else{
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;		
		}

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;	
	}

	Mutex* getMutexByID(TVMMutexID mid){
		for(auto it = muts.begin(); it != muts.end(); ++it){
			if(mid == (*it)->mid){
				return *it;
			}
		}
		return NULL;
	}

	//Pool Stuff

	void insertBlock(MemoryPool *pool, MemoryBlock block){
		for(auto it = pool->memoryBlocks.begin(); it != pool->memoryBlocks.end(); ++it){
			if(block.start < (*it).start){
				pool->memoryBlocks.insert(it, block);
				return;
			}
		}
		//////////////printf("This is bad\n");
	}

	MemoryPool* getMemoryPoolByID(TVMMemoryPoolID mpid){
		for(auto it = memoryPools.begin(); it != memoryPools.end(); ++it){
			if(mpid == (*it)->mpid){
				return *it;
			}
		}
		return NULL;
	}

	uint8_t* findSpace(MemoryPool* pool, MemoryBlock block){
		uint8_t* startPoint = NULL;

		//////////////printf("%d: %d count\n", pool->mpid, (int)pool->memoryBlocks.size());

		for(unsigned int j = 0; j < pool->memoryBlocks.size(); j++){
			//////////////printf("%p to %p\n", pool->memoryBlocks[j].start, pool->memoryBlocks[j].end);
		}

		unsigned int i = 0;
		for(; i < pool->memoryBlocks.size()-1; i++){
			if(pool->memoryBlocks[i+1].start - pool->memoryBlocks[i].end >= block.size){
				startPoint = pool->memoryBlocks[i].end;
				break;
			}
		}
		return startPoint;
	}

	TVMStatus VMMemoryPoolCreate(void *base, TVMMemorySize size, TVMMemoryPoolIDRef memory){
		if(base == NULL || size == 0 || memory == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = new MemoryPool;
		pool->mpid = nextMPID++;
		pool->size = size;
		pool->base = (uint8_t*)base;
		memoryPools.push_back(pool);

		*memory = pool->mpid;

		MemoryBlock block;
		block.end = pool->base;
		block.start = pool->base;
		block.size = 0;

		pool->memoryBlocks.push_back(block);

		MemoryBlock end;
		end.size = 0;
		end.start = pool->base + pool->size;
		end.end = pool->base + pool->size;
		
		pool->memoryBlocks.push_back(end);

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMemoryPoolDelete(TVMMemoryPoolID memory){
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = getMemoryPoolByID(memory);

		if(pool == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}


		if(pool->memoryBlocks.size() > 2){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_STATE;
		}

		memoryPools.erase(std::remove(memoryPools.begin(), memoryPools.end(), pool), memoryPools.end());

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMemoryPoolQuery(TVMMemoryPoolID memory, TVMMemorySizeRef bytesleft){
		if(bytesleft == 0)
			return VM_STATUS_ERROR_INVALID_PARAMETER;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = getMemoryPoolByID(memory);

		if(pool == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		*bytesleft = pool->size;
		for(auto it = pool->memoryBlocks.begin(); it != pool->memoryBlocks.end(); ++it){
			*bytesleft -= (*it).size;
		}

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMemoryPoolAllocate(TVMMemoryPoolID memory, TVMMemorySize size, void **pointer){
		if(size == 0 || pointer == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = getMemoryPoolByID(memory);

		if(pool == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		if(size%64 != 0)
			size = (size + 64) - (size + 64)%64;

		MemoryBlock block;
		block.size = size;
		block.start = findSpace(pool, block);

		if(block.start == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INSUFFICIENT_RESOURCES;
		}

		block.end = block.start+block.size;

		insertBlock(pool, block);
		*pointer = block.start;


		//////////////printf("Allocating in %p to %p\n", block.start, block.end);

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMMemoryPoolDeallocate(TVMMemoryPoolID memory, void *pointer){
		if(pointer == NULL)
			return VM_STATUS_ERROR_INVALID_PARAMETER;

		TMachineSignalState state;
		MachineSuspendSignals(&state);

		MemoryPool *pool = getMemoryPoolByID(memory);

		if(pool == NULL){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		bool found = false;
		for(auto it = pool->memoryBlocks.begin()+1; it != pool->memoryBlocks.end()-1; ++it){
			if(pointer == (*it).start){
				pool->memoryBlocks.erase(it);
				//////////////printf("Deallocating in %p\n", (*it).start);
				found = true;
				break;
			}
		}

		if(!found){
			MachineResumeSignals(&state);
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}


	//Directory commands


	TVMStatus VMDirectoryOpen(const char *dirname, int *dirdescriptor){
		if(dirname == NULL || dirdescriptor == NULL){
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		//Only handle root for now
		if(strcmp(dirname, "/") != 0 && strcmp(dirname, ".") != 0){
			MachineResumeSignals(&state);
			return VM_STATUS_FAILURE;
		}

		*dirdescriptor = ++currentFD;

		DirEnt d;

		d.ID = currentFD;
		d.loc = 0;

		openDirs.push_back(d);

		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMDirectoryClose(int dirdescriptor){
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		for(unsigned int i = 0; i < openDirs.size(); i++){
			if(dirdescriptor == (int)openDirs[i].ID){
				openDirs.erase(openDirs.begin()+i);
				MachineResumeSignals(&state);
				return VM_STATUS_SUCCESS;
			}
		}

		MachineResumeSignals(&state);
		return VM_STATUS_ERROR_INVALID_PARAMETER;
	}


	TVMStatus VMDirectoryRead(int dirdescriptor, SVMDirectoryEntryRef dirent){
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		for(unsigned int i = 0; i < openDirs.size(); i++){
			if(dirdescriptor == (int)openDirs[i].ID){
				while(1){
					if(openDirs[i].loc >= root.size()){
						MachineResumeSignals(&state);
						return VM_STATUS_FAILURE;
					}
					if(root[openDirs[i].loc].DIR_Attr != (0x1 | 0x2 | 0x4 | 0x8)){
						strcpy(dirent->DShortFileName, root[openDirs[i].loc].DIR_Name);
						strcpy(dirent->DLongFileName, dirent->DShortFileName);

						dirent->DSize = root[openDirs[i].loc].DIR_FileSize;
						dirent->DAttributes = root[openDirs[i].loc].DIR_Attr;

						//Get the date/times
						dirent->DCreate.DYear = (root[openDirs[i].loc].DIR_CrtDate >> 9) + 1980;
						dirent->DAccess.DYear = (root[openDirs[i].loc].DIR_LstAccDate >> 9) + 1980;
						dirent->DModify.DYear = (root[openDirs[i].loc].DIR_WrtDate >> 9) + 1980;

						dirent->DCreate.DDay = (root[openDirs[i].loc].DIR_CrtDate & 0x1F);
						dirent->DAccess.DDay = (root[openDirs[i].loc].DIR_LstAccDate & 0x1F);
						dirent->DModify.DDay = (root[openDirs[i].loc].DIR_WrtDate & 0x1F);

						dirent->DCreate.DMonth = (root[openDirs[i].loc].DIR_CrtDate >> 5 & 0x7);
						dirent->DAccess.DMonth = (root[openDirs[i].loc].DIR_LstAccDate >> 5 & 0x7);
						dirent->DModify.DMonth = (root[openDirs[i].loc].DIR_WrtDate >> 5 & 0x7);

						dirent->DCreate.DHour = (root[openDirs[i].loc].DIR_CrtTime >> 11);
						dirent->DModify.DHour = (root[openDirs[i].loc].DIR_WrtTime >> 11);

						dirent->DCreate.DSecond = (root[openDirs[i].loc].DIR_CrtTime & 0x1F);
						dirent->DModify.DSecond = (root[openDirs[i].loc].DIR_WrtTime & 0x1F);

						dirent->DCreate.DMinute = (root[openDirs[i].loc].DIR_CrtTime >> 5 & 0x7);
						dirent->DModify.DMinute = (root[openDirs[i].loc].DIR_WrtTime >> 5 & 0x7);


						openDirs[i].loc++;
						MachineResumeSignals(&state);
						return VM_STATUS_SUCCESS;
					}
					openDirs[i].loc++;
				}
			}
		}

		MachineResumeSignals(&state);
		return VM_STATUS_ERROR_INVALID_PARAMETER;
	}

	TVMStatus VMDirectoryRewind(int dirdescriptor){
		TMachineSignalState state;
		MachineSuspendSignals(&state);

		for(unsigned int i = 0; i < openDirs.size(); i++){
			if(dirdescriptor == (int)openDirs[i].ID){
				openDirs[i].loc = 0;
				MachineResumeSignals(&state);
				return VM_STATUS_SUCCESS;
			}
		}

		MachineResumeSignals(&state);
		return VM_STATUS_ERROR_INVALID_PARAMETER;
	}

	TVMStatus VMDirectoryCurrent(char *abspath){
		if(abspath == NULL){
			return VM_STATUS_ERROR_INVALID_PARAMETER;
		}
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		abspath[0] = '/';
		abspath[1] = '\0';
		MachineResumeSignals(&state);
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMDirectoryChange(const char *path){
		if(strcmp(path, "/") != 0 && strcmp(path, ".") != 0){
			return VM_STATUS_FAILURE;
		}
		return VM_STATUS_SUCCESS;
	}

	TVMStatus VMDirectoryCreate(const char *dirname){
		return VM_STATUS_FAILURE;
	}

	TVMStatus VMDirectoryUnlink(const char *path){
		return VM_STATUS_FAILURE;
	}


	void machineFileWriteCallback(void* calledParam, int result){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		((TCB*)calledParam)->IOResult = result;
		((TCB*)calledParam)->state = VM_THREAD_STATE_READY;
		((TCB*)calledParam)->q = q++;
		readyQueue.push((TCB*)calledParam);

		//////printf("current: %d\n", current->flag);
		
		scheduler();
		MachineResumeSignals(&state);
	}

	void removeFromMutex(TCB* t){
		////////////////printf("%d\n", t->tid);
		if(t->waitingOn != NULL){
			Mutex* m = t->waitingOn;
			if(NULL != m->owner && (*m->owner) == t->tid){
				m->owner = NULL;
				m->locked = false;
			}
			switch(t->prio){
				case VM_THREAD_PRIORITY_LOW:
					m->lowQueue.erase(std::remove(m->lowQueue.begin(), m->lowQueue.end(), t), m->lowQueue.end());
					break;
				case VM_THREAD_PRIORITY_NORMAL:
					m->midQueue.erase(std::remove(m->midQueue.begin(), m->midQueue.end(), t), m->midQueue.end());
					break;
				case VM_THREAD_PRIORITY_HIGH:
					m->highQueue.erase(std::remove(m->highQueue.begin(), m->highQueue.end(), t), m->highQueue.end());
					break;
			}
		}
	}

	void machineAlarmCallback(void* callData){
		TMachineSignalState state;
		MachineSuspendSignals(&state);
		tickCount++;
		current->qUp = 1;
		for(auto it = threads.begin(); it != threads.end(); ++it){
			if(VM_THREAD_STATE_WAITING == (*it)->state){
				if((*it)->mutexWait > 0 && --(*it)->mutexWait == 0){
					(*it)->state = VM_THREAD_STATE_READY;
					(*it)->q = q++;
					removeFromMutex((*it));
					readyQueue.push((*it));
				}
				else if((*it)->sleep != 0 && --(*it)->sleep == 0){
					(*it)->state = VM_THREAD_STATE_READY;
					(*it)->q = q++;
					readyQueue.push((*it));
				}
			}
		}
		scheduler();
		MachineResumeSignals(&state);
	}
}
